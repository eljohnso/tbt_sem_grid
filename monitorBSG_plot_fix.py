import pyjapc
import matplotlib.pyplot as plt
from cern_general_devices import bpm
import datetime
import math
import os
import numpy as np
import pickle

import json
from pybt.myjson.encoder import myJSONEncoder

time_at_script_start = datetime.datetime.now()

userName = "CPS.USER.LHC1"
userNamePSB = "PSB.USER.MD8"
myMonitor = pyjapc.PyJapc(userName, noSet=True)
myMonitor.rbacLogin()

bct_threshold = 5

allOK = False

study_tag = "SEM_GRID_STUDY_"
plot_path = "/user/cpsop/EliottFolder/tbt_sem_grid/23_march/" # When working in the CCC
# plot_path = "/afs/cern.ch/work/e/eljohnso/public/tbt_sem_grid/22_march/" # When working on AFS
file_name = study_tag + str(datetime.datetime.now().strftime("%Y_%m_%d_%Hh_%Mm_%Ss"))
os.mkdir(plot_path + file_name)
print('Data folder = ', plot_path + file_name)

btp_quad_names = ["BTP.QNO20","BTP.QNO30","BTP.QNO35","BTP.QNO50","BTP.QNO55","BTP.QNO60"]
bct_names = ["BTP.BCT10/Acquisition#totalIntensityCh1"]
bpmBT_names = ['BT3.BPM00','BT3.BPM10','BT3.BPM20','BT.BPM30','BT.BPM40','BTP.BPM00','BTP.BPM10','BTP.BPM20','BTP.BPM30','BTP.BPM60']
bpm_properties = ['position','positionH','positionV','bunch_idx','bucket_idx', 'bucketId', 'meanSigma', 'channelNamesH','channelNames','channelNamesV','channelNames']
bpmPS = bpm.BPMOPS(myMonitor, 'PR.BPM', timingSelectorOverride=userName)

bct = {}
btp_quad = {}
freq_psb = {}

bpmPS_data = {}
bpmBT_data = {}
bpmBT_data = {i: {} for i in bpmBT_names}

semgrid_name_list = ['PI.BSFT48','PI.BSFT52','PI.BSFT54']
semgrid_name_dict = {semgrid_name + "/Acquisition": [] for semgrid_name in semgrid_name_list}

iteration_number = 0
quadrupole_scan_bool = False
dispersion_scan_bool = False
nominal_k = 0.471461231
quad_percent = 10  # %
number_of_values_to_scan = 9  # Should be odd
repetition = 1
number_of_freq_to_scan = 3  # Should be odd
nominal_freq = 3166597.
freq_diff = 100 # Hz

if quadrupole_scan_bool == True:
    print("Starting SCAN\n")
    quadrupole_to_scan = "logical.BTP.QNO60/K"
    k_to_scan = np.linspace(nominal_k * (100 - quad_percent) / 100, nominal_k * (100 + quad_percent) / 100,
                            number_of_values_to_scan)  # Max K = 1.000709466718468
    if dispersion_scan_bool == True:
        k_to_scan = np.repeat(k_to_scan, number_of_freq_to_scan)
    k_to_scan = np.repeat(k_to_scan, repetition)
    if dispersion_scan_bool == True:
        freq_to_scan = np.linspace(nominal_freq - freq_diff, nominal_freq + freq_diff,
                                   number_of_freq_to_scan)
        freq_to_scan = np.tile(freq_to_scan, number_of_values_to_scan * repetition)
    print(f"Estimated time for scan = {len(k_to_scan) * 30 / 60} min")

total_turn_to_display = 31 # Max 200
first_turn = 38
plot_dimension = math.ceil(np.sqrt(total_turn_to_display))

fig, ax = plt.subplots(plot_dimension, plot_dimension, sharex=True, sharey=True, tight_layout=True, figsize=(10,10))
fig.canvas.manager.set_window_title(f'TbT SemGrid started at: {time_at_script_start.strftime("%Y/%m/%d %Hh%Mm%Ss")}')
fig.suptitle(f'TbT SemGrid {time_at_script_start.strftime("%Y/%m/%d %Hh%Mm%Ss")}')


def callBack(paramName, newValue, headerData):
    global allOK
    global iteration_number
    checkFirstUpdate = headerData['isFirstUpdate']
    for turn in range(total_turn_to_display):
        ax[turn//plot_dimension, turn%plot_dimension].clear()
    time_at_new_shot = datetime.datetime.now()
    if not checkFirstUpdate:
        for i in bct_names:
            bct[i] = myMonitor.getParam(i, timingSelectorOverride=userNamePSB)
            checkBCT = bct["BTP.BCT10/Acquisition#totalIntensityCh1"]
            print(f"BCT = {checkBCT[0]} / Threshold = {bct_threshold}")

        if checkBCT[0] > bct_threshold:
            if not allOK:
                print(f"\nIteration number: {iteration_number}")
                print(f"New cycle acquired at {headerData['cycleStamp']}")
                allOK = True
                print('Grabbing data PYJAPC...')

                fig.suptitle("New shot at: " + time_at_new_shot.strftime("%Hh%Mm%Ss"))

                freq_psb['PA.EXTFREQPSB/Frequency#frequency'] = myMonitor.getParam("PA.EXTFREQPSB/Frequency#frequency",
                                                                                   timingSelectorOverride=userName)

                print("Freq. sent to PSB ", freq_psb['PA.EXTFREQPSB/Frequency#frequency'])

                for i in btp_quad_names:
                    btp_quad[i + "_MEAS.PULSE#VALUE"] = myMonitor.getParam(i + "/REF.PULSE.AMPLITUDE#VALUE",
                                                                                    timingSelectorOverride=userNamePSB)
                    btp_quad[i + "_K"] = myMonitor.getParam("logical." + i + "/K", timingSelectorOverride=userNamePSB)

                bpmPS_data_temp, bpmPS_data['header'] = bpmPS.getTrajectoryBBB()

                for i in bpm_properties:
                    bpmPS_data[i] = bpmPS_data_temp[i]

                for i in bpmBT_names:
                    bpmBTx = bpm.BPMBT(myMonitor, i, plane='x', override_selector=userNamePSB)
                    bpmBTy = bpm.BPMBT(myMonitor, i, plane='y', override_selector=userNamePSB)
                    bpmBT_data[i]['pos_x'], bpmBT_data['header'] = bpmBTx.getPosition(timingSelectorOverride=userNamePSB)
                    bpmBT_data[i]['pos_y'], _ = bpmBTy.getPosition(timingSelectorOverride=userNamePSB)

                for semgrid_name in semgrid_name_list:
                    print(semgrid_name)
                    semgrid_data = myMonitor.getParam(semgrid_name+"/Acquisition")
                    semgrid_name_dict[semgrid_name+"/Acquisition"].append(semgrid_data)

                    pos = np.array(semgrid_data["projPositionSet1"]).reshape(-1, 31)
                    sem = np.array(semgrid_data["projDataSet1"]).reshape(-1, 31)

                    # Plot SEM Grid
                    if semgrid_name == "PI.BSFT48":
                        for turn in range(total_turn_to_display):
                            ax[turn//plot_dimension,turn%plot_dimension].plot(pos[turn+first_turn], sem[turn+first_turn], c="b", marker="o", ls="", markersize=2, label=f"{semgrid_name}")
                            ax[turn//plot_dimension,turn%plot_dimension].set_title(f"Turn = {turn+first_turn}", fontsize=6)
                    if semgrid_name == "PI.BSFT52":
                        for turn in range(total_turn_to_display):
                            ax[turn//plot_dimension,turn%plot_dimension].plot(pos[turn+first_turn], sem[turn+first_turn], c="r", marker="o", ls="", markersize=2, label=f"{semgrid_name}")
                    if semgrid_name == "PI.BSFT54":
                        for turn in range(total_turn_to_display):
                            ax[turn//plot_dimension,turn%plot_dimension].plot(pos[turn+first_turn], sem[turn+first_turn], c="g", marker="o", ls="", markersize=2, label=f"{semgrid_name}")
                            ax[turn // plot_dimension, turn % plot_dimension].legend(loc="upper right", fontsize=6)

                # CHANGING SETTINGS FOR NEXT SHOT

                if (quadrupole_scan_bool == True):
                    # Set new K1 to converter we scan
                    print(f"Setting {quadrupole_to_scan} k to: {k_to_scan[iteration_number]}")
                    myMonitor.setParam(quadrupole_to_scan, k_to_scan[iteration_number], timingSelectorOverride=userNamePSB)
                    if (dispersion_scan_bool == True):
                        # Set new PSB freq for dispersion
                        print(f"Setting freq to: {freq_to_scan[iteration_number]}")
                        myMonitor.setParam("PA.EXTFREQPSB/Frequency#frequency", freq_to_scan[iteration_number],
                                           timingSelectorOverride=userName)

                try:
                    # Pickle data dump
                    pickle.dump((btp_quad, freq_psb, bct, bpmPS_data, bpmBT_data, semgrid_name_dict),
                                open(plot_path + file_name + "/" + study_tag +str(datetime.datetime.now().strftime("%Y_%m_%d_%Hh_%Mm_%Ss")) + '.pickle', 'wb'))
                    # JSON data dump
                    with open(plot_path + file_name + "/" + study_tag + str(datetime.datetime.now().strftime("%Y_%m_%d_%Hh_%Mm_%Ss")) + '.json', 'w') as outfile:
                        outfile.write(
                            json.dumps((btp_quad, freq_psb, bct, bpmPS_data, bpmBT_data, semgrid_name_dict), cls=myJSONEncoder))
                except:
                    print('Error on saving data json or pickle')

                print('Data saved...')

                # Increment the current to scan
                if quadrupole_scan_bool == True:
                    if iteration_number >= (len(k_to_scan) - 1):
                        print(f"Scan completed")
                        iteration_number = (len(k_to_scan) - 2)
                iteration_number += 1

                # Update the plot
                fig.canvas.draw()

                allOK = False
                print('Waiting for new cycle')
            else:
                print('Error in pyjapc data retrieval')
        else:
            print("Shot ignored on BCT ring intensity")
    else:
        print('Skipping first update!')

myMonitor.subscribeParam('XTIM.PX.ECY-CT/Acquisition#acqC',callBack, getHeader=True,timingSelectorOverride=userName)
myMonitor.startSubscriptions()
fig.show()
print('Wait to ignore first acquisitions...')