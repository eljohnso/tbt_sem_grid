import matplotlib.pyplot as plt
import numpy as np
import datetime
import glob
import jpype
jpype.startJVM(jpype.getDefaultJVMPath())
import pickle
from scipy.optimize import curve_fit
from matplotlib.animation import FuncAnimation

def gaussian_function(x, a, I, mu, sig):
    return a + I / np.sqrt(2 * np.pi * sig ** 2) * np.exp(-(x - mu) ** 2 / 2. / sig ** 2)

def do_gaussian_fit(x,y):
    mu = np.average(x, weights=np.abs(y - np.min(y)))
    sigma = np.sqrt(np.average(x**2, weights=np.abs(y - np.min(y))) - mu**2)
    p0 = [y.min(), (np.max(y) - np.min(y)) * np.sqrt(2 * np.pi * sigma**2), mu, sigma]
    popt, pcov = curve_fit(gaussian_function, x, y, p0=p0, maxfev=1000) # maxfev is the number of tries it does the fit
    return popt, pcov

# Find all files that start with "my_particles"
file_list = glob.glob("to_delete/*")
interesting_file = file_list[-1]

file = glob.glob(f"{interesting_file}/*")

fig, ax = plt.subplots(tight_layout=True, figsize=(8, 5))

def update(i):
    with open(file[0], 'rb') as f:
        data = pickle.load(f)

    ax.clear()

    for instrument in ['PI.BSFT48/Acquisition', 'PI.BSFT52/Acquisition', 'PI.BSFT54/Acquisition']:
        # for cycle in range(len(data[5][instrument])):
        cycle = -1

        turn_range = list(range(0,30))

        pos = np.array(data[5][instrument][cycle]["projPositionSet1"]).reshape(-1, 31)
        sem48 = np.array(data[5][instrument][cycle]["projDataSet1"]).reshape(-1, 31)

        sigma_list = []
        for turn in turn_range:
            try:
                x = pos[turn]
                y = sem48[turn]
                popt, pcov = do_gaussian_fit(x, y)
                sigma_list.append(popt[3])
            except:
                # print("error in gaussian fitting")
                sigma_list.append(0)

        ax.plot(sigma_list, marker="o", label=f"Shot = {cycle}, SemGrid = {instrument}")
        ax.set_xlabel("Turn", fontsize=18)
        ax.set_ylabel("$\sigma$ [mm]", fontsize=18)
        ax.tick_params(axis='x', labelsize=16)
        ax.tick_params(axis='y', labelsize=16)
        ax.set_title(f"{instrument} {data[5][instrument][cycle]['planeSet1'][1]} {datetime.datetime.now()}")
        ax.legend()

ani = FuncAnimation(fig, update, interval=2000)
plt.show()
