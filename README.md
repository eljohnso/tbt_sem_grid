# tbt_sem_grid

This code is used for monitoring the beam in the CERN Proton Synchrotron Booster (PSB) and recording the beam properties using various devices such as BGI, BSG, BPM, etc. The data is saved in a JSON format after each beam acquisition cycle.

The main purpose of this code is to study the beam size of the injected beam in the PS using turn-by-turn SEM grids. The code uses the PyJapc library for interfacing with the CERN accelerator control system.

## Prerequisites
* PyJapc library
* Matplotlib library
* cern_general_devices library
* Pandas library
* Numpy library
* Scipy library

## Usage
* Clone or download the repository to your local machine.
* Install the required libraries.
* Open the monitorBSG.py script in your preferred Python IDE.
* Modify the parameters as needed, such as the file path and study tag.
*Run the script.
The script will continuously monitor the beam and save the data after each beam cycle. The saved data can be further analyzed and plotted using other tools.