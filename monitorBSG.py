import pyjapc
import matplotlib.pyplot as plt
from cern_general_devices import bgi
from cern_general_devices import bsg
from cern_general_devices import bpm
import os
import datetime
from datetime import timedelta
import pytz
import time
import json
import glob
import logging
from pybt.myjson.encoder import myJSONEncoder
from scipy.optimize import curve_fit

# Enable logger output at INFO level and with custom formatting
import logging
logging.basicConfig(format='%(asctime)s|%(name)s[%(process)d]|%(levelname)s - %(message)s')
logger = logging.getLogger()
logger.setLevel(logging.INFO)

import matplotlib.pyplot as plt

import pandas as pd
import os
import numpy as np
import warnings

verbose = False

userName = "CPS.USER.LHCIND1"
userNamePSB = "PSB.USER.LHCIND1"
myMonitor = pyjapc.PyJapc(userName, noSet=False)
myMonitor.rbacLogin()

bct_threshold = 5.0

allOK = False
last_bgi_file = []

study_tag = "BSG_STUDY_TAG_"
plot_path = "/user/cpsop/EliottFolder/tbt_sem_grid/temp/"
file_name = study_tag + str(datetime.datetime.now())

os.mkdir(plot_path + file_name)

print('Data folder = ', plot_path + file_name)

btp_quad_names = ["BTP.QNO20","BTP.QNO30","BTP.QNO35","BTP.QNO50","BTP.QNO55","BTP.QNO60"]

bct_names = ["BTP.BCT10/Acquisition#totalIntensityCh1"]
bpmBT_names = ['BT3.BPM00','BT3.BPM10','BT3.BPM20','BT.BPM30','BT.BPM40','BTP.BPM00','BTP.BPM10','BTP.BPM20','BTP.BPM30','BTP.BPM60']
bsg_names = ['PI.BSFT48','PI.BSFT52','PI.BSFT54']
# bsg_names = ['PI.BSFT54']
bpm_properties = ['position','positionH','positionV','bunch_idx','bucket_idx', 'bucketId', 'meanSigma', 'channelNamesH','channelNames','channelNamesV','channelNames']
bpmPS = bpm.BPMOPS(myMonitor, 'PR.BPM', timingSelectorOverride=userName)

bct = {}
btp_quad = {}
freq_psb = {}

bpmPS_data = {}
bpmBT_data = {}
bpmBT_data = {i: {} for i in bpmBT_names}

bsg_data = {i: {} for i in bsg_names}

iteration_number = 0
quadrupole_scan_bool = True
if quadrupole_scan_bool == True:
    print("Starting SCAN\n")
    quadrupole_to_scan = "logical.BTP.QNO60/K"
    k_to_scan = np.linspace(0.0, 0.471461231, 10)

def callBack(paramName, newValue, headerData):
    global allOK
    global iteration_number
    checkFirstUpdate = headerData['isFirstUpdate']
    if not checkFirstUpdate:
        for i in bct_names:
            bct[i] = myMonitor.getParam(i, timingSelectorOverride=userNamePSB)
            checkBCT = bct["BTP.BCT10/Acquisition#totalIntensityCh1"]

        if checkBCT[0] > bct_threshold:
            if not allOK:
                print(f"Iteration number: {iteration_number}")
                print('New cycle acquired!')
                print('Cycle Stamp = ' + str(headerData['cycleStamp']))
                allOK = True
                print('Check flag', str(allOK))

                print('Grabbing data PYJAPC...')

                freq_psb['PA.EXTFREQPSB/Frequency#frequency'] = myMonitor.getParam("PA.EXTFREQPSB/Frequency#frequency",
                                                                                   timingSelectorOverride=userName)

                print("Freq. sent to PSB ", freq_psb['PA.EXTFREQPSB/Frequency#frequency'])

                if (quadrupole_scan_bool == True):
                    # Set new K1 to converter we scan
                    print(f"Setting {quadrupole_to_scan} k to: {k_to_scan[iteration_number]}")
                    myMonitor.setParam(quadrupole_to_scan, k_to_scan[iteration_number], timingSelectorOverride=userNamePSB)

                for i in btp_quad_names:
                    btp_quad[i + "_REF.PULSE.AMPLITUDE#VALUE"] = myMonitor.getParam(i + "/REF.PULSE.AMPLITUDE#VALUE",
                                                                                    timingSelectorOverride=userNamePSB)
                    btp_quad[i + "_K"] = myMonitor.getParam("logical." + i + "/K", timingSelectorOverride=userNamePSB)

                bpmPS_data_temp, bpmPS_data['header'] = bpmPS.getTrajectoryBBB()

                for i in bpm_properties:
                    bpmPS_data[i] = bpmPS_data_temp[i]

                for i in bpmBT_names:
                    bpmBTx = bpm.BPMBT(myMonitor, i, plane='x', override_selector=userNamePSB)
                    bpmBTy = bpm.BPMBT(myMonitor, i, plane='y', override_selector=userNamePSB)
                    bpmBT_data[i]['pos_x'], bpmBT_data['header'] = bpmBTx.getPosition(timingSelectorOverride=userNamePSB)
                    bpmBT_data[i]['pos_y'], _ = bpmBTy.getPosition(timingSelectorOverride=userNamePSB)

                for i in bsg_names:
                    bsg_device = bsg.BEMTXT(myMonitor, i, timingSelectorOverride=userName, N_turns=30, N_turn1=35)
                    bsg_data[i] = bsg_device.getTbTData()
                    bsg_device.plotTbTfit(noFit=True)
                    #bsg_device.TbTfitting()
                    #bsg_device.plotTbT()

                with open(plot_path + file_name + '/' + str(datetime.datetime.now()) + '.json', 'w') as outfile:
                    outfile.write(
                        json.dumps((btp_quad, freq_psb, bct, bsg_data, bpmPS_data, bpmBT_data), cls=myJSONEncoder))

                print('Data saved...')

                # Increment the current to scan
                if quadrupole_scan_bool == True:
                    if iteration_number >= (len(k_to_scan) - 1):
                        print(f"Scan completed")
                        iteration_number = (len(k_to_scan) - 2)
                iteration_number += 1

                allOK = False

                print('Waiting for new cycle')

            else:
                print('Cycle skipped due to low BCT threshold. Waiting for another!')
        else:
            logging.warning("Shot ignored on BCT ring intenisty")


    else:
        print('Skipping first update!')

myMonitor.subscribeParam('XTIM.PX.ECY-CT/Acquisition#acqC',callBack, getHeader=True,timingSelectorOverride=userName)
# myMonitor.subscribeParam('PR.BCT/HotspotIntensity#dcBefInj1',callBack, getHeader=True,timingSelectorOverride=userName)
myMonitor.startSubscriptions()

print('Wait to ignore first acquisitions...')

