import glob
import json
from datetime import datetime
import matplotlib.pyplot as plt
import os
import time

# Find all folders that start with "SEM_GRID_" in the 22_march directory
folder_list = glob.glob("22_march/SEM_GRID_STUDY*")

# If there are no matching folders, print a message and exit the script
if not folder_list:
    print("No matching folders found.")
    exit()

# Use the last folder in the sorted list of folders
interesting_folder = sorted(folder_list, key=lambda x: datetime.strptime(x.split('/')[-1], "SEM_GRID_STUDY_%Y_%m_%d_%Hh_%Mm_%Ss"))[-1]

print(interesting_folder)

# Initialize variables to store the data
data_list = []
timestamp = []
QNO50_K = []
QNO60_K = []

fig, ax = plt.subplots()

while True:
    # Get a list of all JSON files in the folder, sorted by the datetime contained in their filenames
    file_list = sorted(glob.glob(f"{interesting_folder}/*.json"), key=lambda x: datetime.strptime(x.split('/')[-1].split('.')[0], "SEM_GRID_STUDY_%Y_%m_%d_%Hh_%Mm_%Ss"))

    # Check if there are new JSON files
    if len(file_list) > len(data_list):
        # Loop through each new JSON file
        for file in file_list[len(data_list):]:
            # Load the data from the JSON file
            print(file)
            with open(file, 'rb') as f:
                data = json.load(f)
            data_list.append(data)

            # Extract the timestamp from the data and convert it to a datetime object
            fmt = '%Y-%m-%dT%H:%M:%S.%f%z'
            timestamp.append(datetime.strptime(data[3]["header"]["acqStamp"]["data"], fmt))

            # Extract the QNO50_K and QNO60_K data and append to their respective lists
            QNO50_K.append(data[0]['BTP.QNO50_K'])
            QNO60_K.append(data[0]['BTP.QNO60_K'])

        # Plot the updated data
        ax.clear()
        ax.plot(timestamp, QNO50_K, label="QNO50")
        ax.plot(timestamp, QNO60_K, label="QNO60")
        ax.legend()
        plt.pause(0.01)

    # Wait for 10 seconds before checking for new files again
    time.sleep(10)
